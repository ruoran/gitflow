#!/usr/bin/env python
import json
import requests
import sys

#RedmineToken = "5ede54301457d1b59c6bcd2a8441176f196f5333" # Ruoran
#KanbanToken = "7c654440b824112fab10286f34caaf262445a3e7" # Ruoran
KanbanToken = "a2ec0c8fa8bc559d0bf8b8439611a6c7ffb031c0" # prod kanban token
RedmineToken = "6a0f2ef083dc6c81479904e0916a5b6b53dae1f8" # prod redmine token
RedmineUrl = "projects.prometsupport.com"
KanbanName = "promet" # kanban id

# list of issues need to be skipped
skip_issues = [16334]

def main():
    """ main """
    redmine_add_kanban()
    # TODO: move task in kanban to Done column, etc

def add_to_kanban(issue_id, subject, task_type="bug", project=6880, desc=""):
    """ add a ticket to kanban """
    subject = subject.replace(' ', '%20')
    kanban_task_title = "%23{0}:%20{1}".format(str(issue_id), subject)
    print kanban_task_title.replace("%20", " ")
    print "Task type(Default): ", task_type, " In kanban board: ", project
    print 
    return

    # below code will not be executed, for testing 
    r = requests.post("https://promet.kanbanery.com/api/v1/projects/{0}/tasks.json?task[task_type_name]={1}&task[title]={2}&task[description]={3}".format(project, task_type, kanban_task_title, desc), headers={"X-Kanbanery-ApiToken": KanbanToken})
    print r.status_code
    if r.status_code > 399:
        print r.text
        sys.exit(1)

def get_kanban_task_titles(project=5988):
    """ kanban part """
    requests.get("http://{0}/issues.json?assigned_to_id=146&limit=5000".format(RedmineUrl), headers={"Content-Type":"application/json", "X-Redmine-API-Key":RedmineToken})
    titles = "" 
    r = requests.get("https://promet.kanbanery.com/api/v1/projects/{0}/tasks.json".format(project), headers={"X-Kanbanery-ApiToken": KanbanToken})
    print "Fetched tasks title in kanban: ", r.status_code
    if r.status_code > 399:
        print r.text
        sys.exit(1)

    raw = json.loads(r.text)
    for item in raw:
        titles += item['title']
    return titles

def redmine_add_kanban():
    r = requests.get("http://{0}/issues.json?assigned_to_id=146&limit=5000".format(RedmineUrl), headers={"Content-Type":"application/json", "X-Redmine-API-Key":RedmineToken})

    print "Fetched issues assigned to sysadmin in redmine: ", r.status_code
    if r.status_code > 399:
        print r.text
        sys.exit(1)
    raw = json.loads(r.text)
    titles = get_kanban_task_titles(10209)
    titles += get_kanban_task_titles(6880)
    print 

    for item in raw['issues']:
        if int(item['id']) not in skip_issues and str(item['id']) not in titles:
            project = item['project']['id'] == 441 and "10209" or "6880"
            print "Kanban:",project, " doesn't have ticket ", item['id']
            print
            kanban_task_type = project == "10209" and "backend" or "servers"
            desc = "https://projects.prometsupport.com/issues/{}\n".format(item['id'])
            desc += item['description'] 
            add_to_kanban(int(item['id']), item['subject'], kanban_task_type, project, desc)

if __name__ == '__main__':
    main()
